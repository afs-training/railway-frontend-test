export default {
  'development': {
    api: 'http://localhost:3000'
  },
  'qa': {
    api: 'http://qa:3000'
  },
  'production': {
    api: 'http://production:3000'
  }
}[process.env.REACT_APP_ENV || 'development'];
