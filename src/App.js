import './App.css';
import {Link} from "react-router-dom";
import React from "react";
import config from "./config";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Link to="about">{process.env.REACT_APP_ENV}</Link>
        <Link to="about">{config?.api}</Link>
      </header>
    </div>
  );
}

export default App;
